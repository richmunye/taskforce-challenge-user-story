import React from 'react';
import About from './pages/About';
import Contact from './pages/contactForm';
import Expertise from './pages/expertise';
import HelloSection from './pages/heroSection';
import Navbar from './pages/navbar';
import Footer from './pages/footer';
import { BrowserRouter as Router } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const App = () => (
  <Router>
    <ToastContainer />
    <Navbar />
    <HelloSection />
    <About />
    <Expertise />
    <Contact />
    <Footer />
  </Router>
);

export default App;
