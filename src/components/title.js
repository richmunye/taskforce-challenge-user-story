import React from 'react';

const Title = ({ className, title }) => {
  return (
    <h1 className={`${className} title-content`}>
      {title}
    </h1>
  );
};
export default Title;
