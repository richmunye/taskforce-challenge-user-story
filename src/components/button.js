import React from 'react';

const Button = ({ type, text, className, to }) => {
  return (
    <button
      to={to}
      type={type}
      className={`${className} btns`}
    >
      {text}
    </button>
  );
};
export default Button;
