import React from 'react';

const Content = ({ body, className, body2 }) => {
  return (
    <div>
      <p className={`${className} content`}>{body}</p>
      <p className={`${className} content`}>{body2}</p>
    </div>
  );
};
export default Content;
