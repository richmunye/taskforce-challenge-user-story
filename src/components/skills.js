import React from 'react';

const Skills = ({ src, alt, skill, skillTitle }) => {
  return (
    <div className="skill">
      <img src={src} alt={alt} />
      <h2 className="skill-title">{skillTitle}</h2>
      <ul>
        <li>{skill}</li>
      </ul>
    </div>
  );
};
export default Skills;
