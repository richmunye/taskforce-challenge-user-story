import React from 'react';

const Links = ({ to, className, link }) => {
  return (
    <li className="link">
      <a href={to} className={`${className} link`}>
        {link}
      </a>
    </li>
  );
};
export default Links;
