import React from 'react';

const FormInput = ({
  type,
  name,
  value,
  placeholder,
  label,
  cols,
  rows,
  textArea,
  className,
}) => {
  return (
    <div className="form-input">
      {textArea ? (
        <textarea
          type={type}
          value={value}
          placeholder={placeholder}
          label={label}
          name={name}
          cols={cols}
          rows={rows}
          className={`${className} textarea`}
          required
        />
      ) : (
        <input
          type={type}
          name={name}
          value={value}
          placeholder={placeholder}
          label={label}
          className={`${className} textInput`}
          required
        />
      )}
    </div>
  );
};
export default FormInput;
