import React from 'react';
import Content from '../components/content';
import Title from '../components/title';

const About = () => {
  return (
    <div className="about-container">
      <Title title="About Me" />
      <Content
        body="Hi again! My full name is Kundwa Bruno Materne,
       I am a self taught and passionate full stack software developer 
       based in Rwanda-Kigali ( Africa ). I enjoy solving problems on the 
       web by making progressive web applications using the latest technologies."
        body2="More about myself, I once was a student on freecodecamp certifying
         on Responsive web design, Javascript algorithms and Datastructures, Front End 
         development libraries and API and Micro services. Later on attended a 9 months 
         Software development bootcamp at Andela Rwanda."
      />
    </div>
  );
};
export default About;
