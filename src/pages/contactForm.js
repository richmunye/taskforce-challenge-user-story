import React, { useCallback } from 'react';
import { toast } from 'react-toastify';
import Button from '../components/button';
import FormInput from '../components/formInput';
import Title from '../components/title';

const Contact = () => {
  const handleSubmit = useCallback((event) => {
    const { email, textarea } = event.target.elements;
    event.preventDefault();
    console.log('hello');
    if (email.value === '') {
      return toast.warn('fill in your email');
    }
  });
  return (
    <div className="contact-container">
      <Title title="Contact Me" />
      <form
        className="contact-form"
        onSubmit={handleSubmit}
      >
        <div>
          <input
            type="email"
            name="email"
            placeholder="please enter you email here"
            className="text-input  textInput"
          />
        </div>

        <div style={{ padding: '30px' }}>
          <textarea
            type="text"
            name="textarea"
            placeholder="please enter your message here"
            textArea
            cols="30"
            row="10"
            className="text-area-input  textarea"
          ></textarea>
        </div>

        <button className="contact-btn btns" type="submit">
          Send
        </button>
      </form>
    </div>
  );
};
export default Contact;
