import React from 'react';
import Button from '../components/button';
import Links from '../components/links';

import {
  Redirect,
  useHistory,
  Link,
} from 'react-router-dom';
import {
  FaFacebook,
  FaTwitter,
  FaInstagram,
} from 'react-icons/fa';

const HelloSection = () => {
  return (
    <div className="hero-container">
      <div className="hero-left">
        <div className="brief-describe">
          <p>Hi I'm</p>
          <h2 className="dev-name">
            <strong>John Doe</strong>
          </h2>
          <p>JS Developer</p>
        </div>
        <div className="hero-btns">
          <a
            href="https://mail.google.com/mail/u/0/#inbox?compose=new"
            className="btn-reach-me btns"
          >
            Reach Me
          </a>
          <button
            onClick={() => (
              <Redirect to="http:localhost:8080/anof" />
            )}
            className="btn-cv btns"
          >
            Get Cv
          </button>
        </div>
        <div className="social-media">
          <a href="https://www.facebook.com">
            <FaFacebook />
          </a>

          <Links to="/twitter.com" link={<FaTwitter />} />
          <Links
            to="/instagram.com"
            link={<FaInstagram />}
          />
        </div>
      </div>
      <div className="profile">
        <img
          src="../assets/profile-pic.png"
          alt="profile"
        />
      </div>
    </div>
  );
};
export default HelloSection;
