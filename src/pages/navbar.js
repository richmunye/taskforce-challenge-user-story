import React, { useState, useEffect } from 'react';
import Button from '../components/button';
import Links from '../components/links';

class Navbar extends React.Component {
  constructor() {
    super();
    this.state = {
      show: true,
      scroll: 0,
    };
  }
  componentDidMount() {
    window.addEventListener('scroll', this.controlScroll);
  }
  componentWillUnmount() {
    window.removeEventListener(
      'scroll',
      this.controlScroll
    );
  }
  controlScroll = () => {};
  controlScroll = () => {
    this.setState({
      scroll: document.body.getBoundingClientRect().top,
      show:
        document.body.getBoundingClientRect().top >
        this.state.scroll,
    });
  };
  render() {
    return (
      <nav
        className={`navbar-container ${
          this.state.show ? 'active' : 'hidden'
        }`}
      >
        <h1>Bruno</h1>
        <ul className="navbar-links">
          <Links to="/" link="Home" />
          <Links to="/about" link="About" />
          <Links to="/contact" link="Contact" />
          <Button
            className="signIn"
            type="submit"
            text="SignIn"
          />
        </ul>
      </nav>
    );
  }
}

export default Navbar;
