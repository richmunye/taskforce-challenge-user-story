import React from 'react';
import Title from '../components/title';
const Expertise = () => {
  return (
    <div className="">
      <Title title="My Experience" />
      <div className="expertise">
        <div className="expertise1">
          <img
            src="../assets/expertise1.svg"
            alt="my experience"
          />
          <div>
            <div className="experience">
              <h2>Andela</h2>
              <div className="experience1">
                <h4>Project work</h4>
                <p>from 1/4/2018 to 4/8/20219</p>
              </div>
              <p>
                Worked with a team of developers on a
                fullstack project with a stack of
                techologies involving - Reactjs , Postgres ,
                nodejs, sass.
              </p>
            </div>
            <div className="experience">
              <div className="experience1">
                <h4>Project work</h4>
                <p>from 1/4/2018 to 4/8/20219</p>
              </div>
              <p>
                Worked with a team of developers on a
                fullstack project with a stack of
                techologies involving - Reactjs , Postgres ,
                nodejs, sass.
              </p>
            </div>
          </div>
        </div>
        <div className="expertise2">
          <img
            src="../assets/expertise2.svg"
            alt="my experience"
          />
          <div className="experience">
            <h2>AwesomityLab</h2>
            <div className="experience1">
              <h4>bootcamp</h4>
              <p>from 1/4/2021 to Now</p>
            </div>
            <p>
              Worked with a team of developers on a
              fullstack project with a stack of techologies
              involving - Reactjs , Redux , nodejs, sass.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Expertise;
